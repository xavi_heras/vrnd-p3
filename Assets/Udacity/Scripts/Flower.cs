﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Collectible item (flower)
public class Flower : MonoBehaviour {
	//FlowerPoofPrefab particle effects
	public GameObject flowerPoofPrefab;
	//list of posible flower meshes
	public List <Mesh> flowers = new List <Mesh>();
	//list of particle effects
	public List <GameObject> flowersPoofs = new List <GameObject>();
	//flower type (color, shape)
	private int flowerIndex;

	// Use this for initialization
	void Start () {
		//get a random number to make flowers more variated
		flowerIndex = Random.Range (0, flowers.Count);
		//asign correct mesh to the flower
		GetComponent<MeshFilter> ().mesh = flowers [flowerIndex];
		//we dont need flowers catalog anymore, clear it
		flowers.Clear ();
		//small random rotation
		float r = Random.Range (0.7f, 1f);
		this.transform.localScale = new Vector3 (r,r,r);
	}

	public void OnFlowerClicked() {
		//Increase collected items score and update final scoreboard  (UI)
		Maze.Instance.collectedItems++;
		Maze.Instance.UpdateScore();
		// Instantiate poof Prefab where this flower is located, this new prefab plays a collected sound effect automatically
		Instantiate (flowersPoofs[flowerIndex], this.transform.position, Quaternion.identity);
		// Destroy this flower
		Destroy (this.gameObject);
	}

}
