﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

//Singleton class to control the maze
public class Maze : MonoBehaviourSingleton<Maze> {

#if UNITY_EDITOR
	//development variables
	public bool startFromBeginning = false;
	public GameObject startPoint;
	public GameObject sculpturePoint;
	public GameObject keyPoint;
	public GameObject doorPoint;
	public GameObject endPoint;
	public Texture2D nocursor;
#endif

	//control numer of flowers collected
	public int collectedItems = 0;

	//variable with the current waypoint
	public Waypoint currentPoint = null;

	//maze key gameobject, used to enable/disable key particle effects to improve performace
	public GameObject mazeKey;

	//reference to score board UI text to update number of collected items
	public TextMesh scoreText;

	//fade in- fade out image
	public Image fade;

	//flag to control reload 
	private bool reloading = false;

	// Use this for initialization
	void Start () {
		
#if UNITY_EDITOR


		//Cursor.SetCursor (nocursor, Vector2.zero, CursorMode.Auto);
		//Cursor.lockState = CursorLockMode.Locked;
		//Cursor.visible = false;

		if (startFromBeginning) {
			Camera.main.transform.position = startPoint.transform.position;
			currentPoint = startPoint.GetComponent<Waypoint> ();
			MoveToWaypoint (currentPoint);
		}
#endif
		//force fade initial color/alpha
		fade.color = new Color (fade.color.r,fade.color.g,fade.color.b,1);
		FadeOut (6f);

		//at the start of the maze set collected items to zero
		collectedItems = 0;
		//udate score text
		UpdateScore();
	}
	
	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
//development input
		if (Input.GetKeyDown(KeyCode.Alpha1)) Camera.main.transform.position 	= startPoint.transform.position;
		else if (Input.GetKeyDown(KeyCode.Alpha2)) Camera.main.transform.position 	= sculpturePoint.transform.position;
		else if (Input.GetKeyDown(KeyCode.Alpha3)) Camera.main.transform.position 	= keyPoint.transform.position;
		else if (Input.GetKeyDown(KeyCode.Alpha4)) Camera.main.transform.position 	= doorPoint.transform.position;
		else if (Input.GetKeyDown(KeyCode.Alpha5)) Camera.main.transform.position 	= endPoint.transform.position;
		else if (Input.GetKeyDown(KeyCode.R)) Reload(3);
		else if (Input.GetKeyDown(KeyCode.I)) FadeIn(1.5f);
		else if (Input.GetKeyDown(KeyCode.O)) FadeOut(1.5f);
#endif
	}


	public void MoveToWaypoint (Waypoint point){
		//update current waypoint and 
		if (currentPoint == null)
			currentPoint = point;
	
		//if the user moves to a new waypoint, show last one and hide current one
		if (point != currentPoint) {
			currentPoint.Show ();
			currentPoint = point;
			currentPoint.Hide ();
		}
			
		//particles are very expensive.... this is a small performace boost 
		//check distance to key and activate particles only if the user is near
		//check if the maze key exists
		if (mazeKey != null) {		
			if (Vector3.Distance (Camera.main.transform.position, mazeKey.transform.position) < 6f) {
				//is near the maze key activate particles 	
				mazeKey.GetComponent<MeshRenderer> ().enabled = true;
				GameObject.Find ("MazeKey/KeyParticles/light").SetActive (true);
				GameObject.Find ("MazeKey/KeyParticles/black").SetActive (true);
				GameObject.Find ("MazeKey/KeyParticles/sparks").SetActive (true);
			} else {
				//user is far away, disable particles
				mazeKey.GetComponent<MeshRenderer> ().enabled = false;
				GameObject.Find ("MazeKey/KeyParticles/light").SetActive (false);
				GameObject.Find ("MazeKey/KeyParticles/black").SetActive (false);
				GameObject.Find ("MazeKey/KeyParticles/sparks").SetActive (false);
			}
		}
	
	}

	public void UpdateScore(){
		//update UI score board text
		scoreText.text = "Flowers collected : " + collectedItems  + " / 7";
	}

	private void FadeIn(float time = 1.5f)
	{
		DOTween.To(()=> fade.color, x=> fade.color = x, new Color (fade.color.r,fade.color.g,fade.color.b,1), time).SetOptions(true);
	}

	private void FadeOut(float time = 1.5f)
	{		
		DOTween.To(()=> fade.color, x=> fade.color = x, new Color (fade.color.r,fade.color.g,fade.color.b,0), time).SetOptions(true);
	}

	public void Reload(float time){
		if (!reloading) {
			reloading = true;
			FadeIn (time);
			StartCoroutine (FadeAndReload (time));
		}
	}

	public IEnumerator FadeAndReload(float time){
		yield return new WaitForSeconds (time);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}
}
