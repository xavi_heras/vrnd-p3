﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Key : MonoBehaviour 
{
	//flag to control maze key
	public bool keyCollected = false;
    //reference to the maze door
	public GameObject door;
	//key floating strength
	public float floatingStrength = 1f;

	void Update()
	{
		//Key Floating Animation 
		this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y + (Mathf.Sin(Time.time) * floatingStrength), this.transform.position.z);
	}

	public void OnKeyClicked()
	{
		// Call the Unlock() method on the Door
		door.GetComponent<Door>().Unlock();

		//change key flag
		this.keyCollected = true;

        //De-atach particles from key, stop particle effects and kill emitters 5 seconds later
		var main = GameObject.Find ("KeyParticles/light").GetComponent<ParticleSystem> ().main;
		main.loop = false;
		main.simulationSpeed = 3f;
		main = GameObject.Find ("KeyParticles/black").GetComponent<ParticleSystem> ().main;
		main.loop = false;
		main.simulationSpeed = 3f;
		main = GameObject.Find ("KeyParticles/sparks").GetComponent<ParticleSystem> ().main;
		main.loop = false;
		main.simulationSpeed = 3f;
		GameObject.Find("KeyParticles").transform.SetParent(this.transform.parent);

		DOTween.To(()=> GameObject.Find("KeyParticles").GetComponent<AudioSource>().volume, 
			x=> GameObject.Find("KeyParticles").GetComponent<AudioSource>().volume = x, 0f, 1.5f);

		Destroy(GameObject.Find("KeyParticles"),5f);

		//collect key animation, sound and destroy
		this.GetComponent<AudioSource> ().Play ();

		Sequence keyAnimationSequence = DOTween.Sequence ();
		keyAnimationSequence.Append (transform.DOMove  (new Vector3 (Camera.main.transform.position.x, .5f, Camera.main.transform.position.z),.50f));
		keyAnimationSequence.Join (transform.DOScale (new Vector3 (),.75f));
		keyAnimationSequence.Join (transform.DORotate(new Vector3 (Random.Range (100f,1000f), Random.Range (100f,1000f),Random.Range (100f,1000f)),1f,RotateMode.Fast));
		keyAnimationSequence.OnComplete(()=>Destroy (this.gameObject));

    }

}
