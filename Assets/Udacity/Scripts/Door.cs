﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour 
{
    // Create a boolean value called "locked" that can be checked in OnDoorClicked() 
	public bool locked = true;
    
	private Animator doorAnimator;

	[Header("Door Sounds")]
	public AudioClip clip_locked = null;				
	public AudioClip clip_open = null;		

	[Header("Door Animations")]
	public AnimationClip Idle;
	public AnimationClip Locked;
	public AnimationClip Open;

	void Start() {
		doorAnimator = GetComponent<Animator>();
	}

    public void OnDoorClicked() {		
        //If the door is clicked and unlocked
		if (!locked) {
			//animate the door
			doorAnimator.Play (Open.name);
			//play the unlock sound
			this.GetComponent<AudioSource> ().PlayOneShot (clip_open);
			//destroy box collider 3 seconds later
			Destroy (GetComponent<BoxCollider> (), 3f);
		} else {
			//play locked animation
			doorAnimator.Play (Locked.name);
			//play locked sound
			this.GetComponent<AudioSource> ().PlayOneShot (clip_locked);
		}
    }

    public void Unlock()
    {
        //unlock the door when the maze key is collected
		locked = false;
    }
}
