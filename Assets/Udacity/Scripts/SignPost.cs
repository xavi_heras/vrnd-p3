﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

//Class to control and animate the visibility of end menu
public class SignPost : MonoBehaviour
{	
	//text for contratulations text
	public TextMesh title;
	//text for score
	public TextMesh score;
	//text for restart
	public TextMesh restart;

	void Awake (){
		//all UI elements alpha to 0 by default
		Exit();
	}

	public void Enter()
	{
		//if the user look at the menu, show elements
		DOTween.To(()=> title.color, x=> title.color = x, new Color (title.color.r,title.color.g,title.color.b,1), 1.5f).SetOptions(true);
		DOTween.To(()=> score.color, x=> score.color = x, new Color (score.color.r,score.color.g,score.color.b,1), 1.5f).SetOptions(true);
		DOTween.To(()=> restart.color, x=> restart.color = x, new Color (restart.color.r,restart.color.g,restart.color.b,1), 1.5f).SetOptions(true);

	}
		
	public void Exit()
	{
		//if the user is not looking the menu, fade all content
		DOTween.To(()=> title.color, x=> title.color = x, new Color (title.color.r,title.color.g,title.color.b,0), 1.5f).SetOptions(true);
		DOTween.To(()=> score.color, x=> score.color = x, new Color (score.color.r,score.color.g,score.color.b,0), 1.5f).SetOptions(true);
		DOTween.To(()=> restart.color, x=> restart.color = x, new Color (restart.color.r,restart.color.g,restart.color.b,0), 1.5f).SetOptions(true);

	}

	public void ResetScene() 
	{
		// Reset the scene when the user clicks the sign post
		Maze.Instance.Reload (3);
	}

}