﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

//Class to control and animate the visibility of application main menu
public class MainMenu : MonoBehaviour {

	//udacity logo image
	public Image logo;
	//text for project description
	public TextMesh project;
	//text for degree description
	public TextMesh degree;
	//text for audio, heaphones
	public TextMesh recomend;

	void Awake (){
		//all UI elements alpha to 0 by default
		Exit();
	}

	public void Enter()
	{
		//if the user look at the menu, show elements
		DOTween.To(()=> logo.color, x=> logo.color = x, new Color (1,1,1,1), 1.5f).SetOptions(true);
		DOTween.To(()=> project.color, x=> project.color = x, new Color (1,1,1,1), 1.5f).SetOptions(true);
		DOTween.To(()=> degree.color, x=> degree.color = x, new Color (1,1,1,1), 1.5f).SetOptions(true);
		DOTween.To(()=> recomend.color, x=> recomend.color = x, new Color (1,1,1,1), 1.5f).SetOptions(true);
	}
		
	public void Exit()
	{
		//if the user is not looking the menu, fade all content 
		DOTween.To(()=> logo.color, x=> logo.color = x, new Color (1,1,1,0), 1.5f).SetOptions(true);
		DOTween.To(()=> project.color, x=> project.color = x, new Color (1,1,1,0), 1.5f).SetOptions(true);
		DOTween.To(()=> degree.color, x=> degree.color = x, new Color (1,1,1,0), 1.5f).SetOptions(true);
		DOTween.To(()=> recomend.color, x=> recomend.color = x, new Color (1,1,1,0), 1.5f).SetOptions(true);
	}
}
