A Maze by Xavi Heras

Unity version: 5.5.0f3
Platform: Android
Test device: Pixel XL


For this project I wanted to learn more about performance and custom models. For the maze I took a reference from my childhood, a real labyrinth near my home (https://en.wikipedia.org/wiki/Parc_del_Laberint_d%27Horta). Decided to use a low poly style and used Blender to model all the scene objects. To improve performace I reduced the number of polygons as much as possible and avoided to use lots of materials. 

 Included features:

	- Full custom models and sounds
	- Collectible flowers (7 in total)
	- Custom navigation system 
	- Positional sounds
	- Particles
	- Occlusion to improve performance
	- Birds!


At the end took me around 100 hours to finish this project. Most of the time was used in blender modeling/optimizing scene and baking lights. 

What I liked: Accomplish a real feeling of being inside the maze.  

What was challenging: Performance. performance and performance. I wanted to keep total models triangle count bellow 15k and that was really hard. 


Video walkthrough: https://www.youtube.com/watch?v=y-2t-nopx_Q